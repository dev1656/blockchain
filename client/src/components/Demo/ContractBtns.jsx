import { useState } from "react";
import useEth from "../../contexts/EthContext/useEth";

function ContractBtns({ setValue }) {
  const { state: { contract, accounts } } = useEth();
    const [inputValue, setInputValue] = useState("");
    const [inputValue2, setInputValue2] = useState("");

    const handleInputChange = e => {
        setInputValue(e.target.value);
    };
    const handleInputChange2 = e => {
        setInputValue2(e.target.value);
    };

  const getFruits = async () => {
    const value = await contract.methods.getFruits().call({ from: accounts[0] });
    setValue(value);
  };

    const addFruit = async e => {
        if (e.target.tagName === "INPUT") {
            return;
        }
        await contract.methods.addFruit(inputValue, 1, accounts[0]).send({ from: accounts[0] });
    };

    const buyFruit = async e => {
        if (e.target.tagName === "INPUT") {
            return;
        }
        await contract.methods.buyFruit(inputValue2, accounts[0]).send({ from: accounts[0] });
    };

    const getMyFruits = async () => {
        const value = await contract.methods.getMyFruits(accounts[0]).call({ from: accounts[0] });
        setValue(value);
    }

  return (
    <div className="btns">

        <button onClick={getFruits}>
            getFruits()
        </button>
        <button onClick={getMyFruits}>
            getMyFruits()
        </button>

        <div onClick={addFruit} className="input-btn">
            addFruit(<input
            type="text"
            placeholder="string"
            value={inputValue}
            onChange={handleInputChange}
        />)
        </div>

        <div onClick={buyFruit} className="input-btn">
            buyFruit(<input
            type="text"
            placeholder="string"
            value={inputValue2}
            onChange={handleInputChange2}
        />)
        </div>

    </div>
  );
}

export default ContractBtns;
