// SPDX-License-Identifier: MIT
pragma solidity 0.8.14;

contract Fruits {
    struct Fruit {
        string name;
        uint256 price;
        address owner;
    }

    Fruit[] fruitsToSell;
    mapping(address => Fruit[]) public myFruits;

    function addFruit(string memory fruitName, uint256 price, address owner) public {
        Fruit memory fruit = Fruit(fruitName, price, owner);
        fruitsToSell.push(fruit);
    }

    function buyFruit(string memory fruitName, address owner) public returns (bool) {
        for(uint i=0; i < fruitsToSell.length-1; i++){
            if ((keccak256(abi.encodePacked(fruitsToSell[i].name)) == keccak256(abi.encodePacked(fruitName)))) {
                myFruits[owner].push(fruitsToSell[i]);
                delete fruitsToSell[i];
            }
        }

        return true;
    }

    function getFruits() public view returns (Fruit[] memory) {
        return fruitsToSell;
    }

    function getMyFruits(address owner) public view returns (Fruit[] memory) {
        return myFruits[owner];
    }
}